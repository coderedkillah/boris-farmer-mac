## Boris Farmer Mac

## How do I install?

To install Boris Farmer Mac, you will need to have [Homebrew](brew.sh) and [Homebrew Cask](https://caskroom.github.io/). When you have both installed, type this into a Terminal window:

`brew cask install boris-farmer`

This will install Boris Farmer Mac into your Applications folder. Enjoy!
